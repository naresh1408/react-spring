package com.myportal.myportal.dao;

import com.myportal.myportal.entity.AddressBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface AddressBookRepository extends JpaRepository<AddressBook, String>  {
}
