package com.myportal.myportal.exception;

public enum ErrorCode {
    USER_NOT_FOUND(400,"E400","User not found"),
    PASS_NOT_MATCHED(400,"E400","password not matched"),
    USER_ALREADY_REG(400,"E400","User Already registered");
    private String code;
    private String msg;
    private int httpCode;

    ErrorCode(int httpCode,String code, String message) {
        this.code = code;
        this.msg = message;
        this.httpCode = httpCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }
}
