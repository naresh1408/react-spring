package com.myportal.myportal.exception;

public class PortalException extends RuntimeException {

    private final ErrorCode errorCode;

    public PortalException(String exception, ErrorCode errorCode) {
        super(exception);
        this.errorCode = errorCode;
    }

    public PortalException(ErrorCode errorCode) {
        super(errorCode.getMsg());
        this.errorCode = errorCode;
    }

    public PortalException(String message, ErrorCode errorCode,Throwable exception) {
        super(message,exception);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
