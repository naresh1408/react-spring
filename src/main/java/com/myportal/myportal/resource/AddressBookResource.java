package com.myportal.myportal.resource;

import com.myportal.myportal.entity.AddressBook;
import com.myportal.myportal.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 */
@RestController
@RequestMapping("/address-books")
public class AddressBookResource {

    @Autowired
    private AddressBookService addressBookService;


    @GetMapping
    public ResponseEntity<List<AddressBook>> getAllAddressBooks() {
        List<AddressBook> list = addressBookService.getAllEmployees();
        return new ResponseEntity<List<AddressBook>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AddressBook> getAddressBookById(@PathVariable("id") String id)
            throws Exception {
        AddressBook entity = addressBookService.getAddressById(id);

        return new ResponseEntity<AddressBook>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<AddressBook> createOrUpdateEmployee(@RequestBody AddressBook addressBook)
            throws Exception {
        AddressBook updated = addressBookService.createOrUpdateAddressBook(addressBook);
        return new ResponseEntity<AddressBook>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public HttpStatus deleteAddressBookById(@PathVariable("id") String id)
            throws Exception {
        addressBookService.deleteEmployeeById(id);
        return HttpStatus.FORBIDDEN;
    }
}
