package com.myportal.myportal.resource;

import com.myportal.myportal.entity.User;
import com.myportal.myportal.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserResource {

    @Autowired
    private UserService userService;

    /**
     *
     * @param user
     * @return
     * @throws Exception
     */
    @PostMapping
    public ResponseEntity<User> createOrUpdateEmployee(@RequestBody User user)
            throws Exception {
        User updated = userService.createUser(user);
        return new ResponseEntity<User>(updated, new HttpHeaders(), HttpStatus.OK);
    }

}
