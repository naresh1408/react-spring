package com.myportal.myportal.service;

import com.myportal.myportal.dao.AddressBookRepository;
import com.myportal.myportal.entity.AddressBook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 */
@Service
public class AddressBookService {

    @Autowired
    private AddressBookRepository addressBookRepository;

    public List<AddressBook> getAllEmployees() {
        List<AddressBook> addressBooks = addressBookRepository.findAll();
        if (addressBooks.size() > 0) {
            return addressBooks;
        } else {
            return new ArrayList<AddressBook>();
        }
    }

    public AddressBook createOrUpdateAddressBook(AddressBook entity) {

        Optional<AddressBook> addressBook = addressBookRepository.findById(entity.getId());

        if (addressBook.isPresent()) {
            AddressBook newEntity = addressBook.get();
            newEntity.setEmail(entity.getEmail());
            newEntity.setName(entity.getName());
            newEntity.setSurname(entity.getSurname());
            newEntity.setPhone(entity.getPhone());
            newEntity.setZipcode(entity.getZipcode());
            newEntity.setUserId(entity.getUserId());
            newEntity.setAddress(entity.getAddress());

            newEntity = addressBookRepository.save(newEntity);
            return newEntity;
        } else {
            entity = addressBookRepository.save(entity);
            return entity;
        }
    }

    public AddressBook getAddressById(String id) throws Exception {

        Optional<AddressBook> addressBook = addressBookRepository.findById(id);

        if(addressBook.isPresent()) {
            return addressBook.get();
        } else {
            throw new Exception("No employee record exist for given id");
        }
    }

    /**
     * @param id
     * @throws Exception
     */
    public void deleteEmployeeById(String id) throws Exception {

        Optional<AddressBook> addressBookOptional = addressBookRepository.findById(id);

        if (addressBookOptional.isPresent()) {
            addressBookRepository.deleteById(id);
        } else {
            throw new Exception("No employee record exist for given id");
        }
    }

}
