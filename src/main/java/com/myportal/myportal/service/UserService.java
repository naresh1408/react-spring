package com.myportal.myportal.service;

import com.myportal.myportal.dao.UserRepository;
import com.myportal.myportal.entity.User;
import com.myportal.myportal.exception.PortalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.myportal.myportal.exception.ErrorCode.*;


@Component
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User findByEmail(String email) {
        User user = userRepository.findByEmail(email);
        return user;
    }

    public User createUser(User userEntity) {
        User user = userRepository.findByEmail(userEntity.getEmail());
        if(!userEntity.getPassword().equals(userEntity.getPasswordConfirm())) {
            throw new PortalException(PASS_NOT_MATCHED);
        }
        if(user != null) {
            throw new PortalException(USER_ALREADY_REG);
        } else {
            userEntity = userRepository.save(userEntity);
        }
        userEntity.setPassword("");
        userEntity.setPasswordConfirm("");
        return userEntity;
    }

    public User updateUser(User userEntity) {
        User user = userRepository.findByEmail(userEntity.getEmail());
        if(user != null) {
            User userUpdate = user;
            userUpdate.setName(userEntity.getName());
            userRepository.save(userUpdate);
        } else {
            throw new PortalException(USER_NOT_FOUND);
        }
        return userEntity;
    }
}
